﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jenkin_test.Startup))]
namespace jenkin_test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
